package statistics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Blackjack {

	class Card {
		Integer value;
		String suit;

		public Card(String suit, Integer value) {
			this.value = value;
			this.suit = suit;
		}
	}

	final List<String> suits = Arrays.asList("Spade", "Heart", "Diamond", "Club");
	final List<Integer> values = Arrays.asList(11, 10, 10, 10, 10, 9, 8, 7, 6, 5, 4, 3, 2);

	List<Card> deal(int numDecks) {
		List<Card> deck = new ArrayList<>(suits.size() * values.size() * numDecks);
		for (int i = 0; i < numDecks; i++) {
			suits.forEach((suit) -> {
				values.forEach((value) -> {
					deck.add(new Card(suit, value));
				});
			});
		}
		return deck;
	}
	
	

	/**
     * with random integer picking, the advantage of one deck vs 6 was:
     * 
     * <pre>
     *  0.0008808 = .04830910 - .04742830 
     *  0.0007921 = .04828640 - .04749430
     *  0.0007714 = .04825580 - .04748440
     * 
     * average delta between 1 and 6 decks is: 0.000814767
     * </pre>
     * 
     */
	int pickTwoRandomCards(List<Card> deck) {
		int cardTotal = 0;

		List<Integer> pickedNumbers = new ArrayList<>();
		for (int i = 0; i < 2; i++) {
			int randomNumber;
			do {
				randomNumber = ThreadLocalRandom.current().nextInt(deck.size());
			} while (pickedNumbers.contains(randomNumber));
			pickedNumbers.add(randomNumber);
			cardTotal += deck.get(randomNumber).value;

		}
		return cardTotal;
	}

	/** Returns whether blackjack is gotten from random order of the given deck of cards (input deck order doesn't matter)
	public boolean didGetBlackjackFromTwoRandomCards(List<Card> deck) {
		return pickTwoRandomCards(deck) == 21;
	}
	
	/** Expects an already-shuffled deck and returns true if a 2 card deal results in blackjack */
	boolean getBlackjackFromShuffledDeck(List<Card> deck) {
	    return (deck.get(0).value + deck.get(1).value) == 21;
	}
	/** 
	 * <pre>
	 * 
        With shuffled decks each round:
        0.0008394 = .04829860 - .04745920
        0.0007765 = .04824250 - .04746600
        0.0007599 = .04823530 - .04747540
        
        average delta between 1 and 6 decks with full shuffling: 0.000791933
        </pre>
	 */

}
