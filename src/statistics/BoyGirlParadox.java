package statistics;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


public class BoyGirlParadox {

    class Child {
        String gender;
        Child(String gender) {
            this.gender = gender;
        }
    }
    
    /**
     * Mr. Jones has two children. The older child is a girl. What is the probability that both children are girls?
     */
    public boolean areBothMrJonesKidsGirls() {
        List<Child> gg = Arrays.asList(new Child("Girl"), new Child("Girl"));
        List<Child> gb = Arrays.asList(new Child("Girl"), new Child("Boy"));
        List<List<Child>> possibleKids = Arrays.asList(gg, gb);
        
        List<Child> jonesKids = possibleKids.get(ThreadLocalRandom.current().nextInt(possibleKids.size()));
        return !jonesKids.stream().anyMatch((kid) -> kid.gender.equals("Boy"));
    }
    
    /** 
     * Mr. Smith has two children. At least one of them is a boy. What is the probability that both children are boys?
     */
    public boolean areBothMrSmithsKidsBoys() {
        List<Child> bb = Arrays.asList(new Child("Boy"), new Child("Boy"));
        List<Child> bg = Arrays.asList(new Child("Boy"), new Child("Girl"));
        List<Child> gb = Arrays.asList(new Child("Girl"), new Child("Boy"));
        List<List<Child>> possibleKids = Arrays.asList(bb, bg, gb);
        
        List<Child> smithKids =possibleKids.get(ThreadLocalRandom.current().nextInt(possibleKids.size())); 
        return !smithKids.stream().anyMatch((child) -> child.gender.equals("Girl"));
    }
    
}
