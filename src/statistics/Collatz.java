package statistics;

import java.util.ArrayList;
import java.util.List;

public class Collatz {

	public List<Integer> getCollatzNumbers(int number) {
		List<Integer> numbers = new ArrayList<>();
		int result = number;
		do {
			if (result % 2 == 0) {
				result /= (double) 2;
			} else {
				result = (result * 3) + 1;
			}
			numbers.add(result);
		} while (result != 1);
		return numbers;
	}
}
