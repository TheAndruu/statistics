package statistics;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class CollegeFund {

	double tuitionIncreaseRate = 0.05;
	double currentCollegeCost = 23000d;

	protected static final DecimalFormat df = new DecimalFormat("#,###,###.00");
	protected static final DecimalFormat percent = new DecimalFormat(".00");
	// Can calculate a monthly interest rate that varies within a deviation of a 7%
	// return, then run scenario multiple times to see where it falls

	// Can add scenario with withdrawing to pay for k-12

	class Breakdown {
		double balance;
		LocalDate date;
		double annualReturn;
		double annualRate;
		double totalContribution;
		double totalGrowth;

		@Override
		public String toString() {
			return date.toString() + ": $" + df.format(balance) + ", contrib: $" + df.format(totalContribution) + ", growth: $" + df.format(totalGrowth)
					+ ", annualReturn $" + df.format(annualReturn) + ",APY: " + percent.format(annualRate);
		}
	}

	// 14 yrs til eva goes to college
	// takes a random rate of return between -7 and 14%
	public List<Breakdown> getBreakdown(double initialInvestment, double monthlyAddition, int yearsToAccrue) {
		LocalDate date = LocalDate.of(2019, 01, 01);
		double balance = initialInvestment;
		double contributionAmount = initialInvestment;
		List<Breakdown> breakdown = new ArrayList<>(12 * yearsToAccrue);

		for (int i = 0; i < yearsToAccrue; i++) {
			// get random interest rate for the year, between -7 and 14%
			double annualRate = ThreadLocalRandom.current().nextInt(-7, 15) / 100d;
			double monthlyRate = annualRate / 12d;
			
			double monthPerformance = 0;
			for (int j = 0; j < 12; j++) {
				monthPerformance += balance * monthlyRate;
				balance = balance + balance * monthlyRate + monthlyAddition;
				contributionAmount += monthlyAddition;

			}
			Breakdown thisMonth = new Breakdown();
			thisMonth.date = date;
			thisMonth.balance = balance;
			thisMonth.annualReturn = monthPerformance;
			thisMonth.annualRate = annualRate;
			thisMonth.totalContribution = contributionAmount;
			thisMonth.totalGrowth = balance - contributionAmount;
			breakdown.add(thisMonth);
			date = date.plusYears(1);
		}
		return breakdown;

	}
}
