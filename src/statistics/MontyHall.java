package statistics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class MontyHall {

    class Door {
        String contents;
        Door(String contents) {
            this.contents = contents;
        }
    }
    
    List<Door> random3Doors() {
        List<Door> doors = new ArrayList<>();
        doors.add(new Door("Goat"));
        doors.add(new Door("Goat"));
        doors.add(new Door("Prize"));
        Collections.shuffle(doors);
        return doors;
    }
    
    public boolean didGetPrizeChangingAfterReveal() {
        List<Door> doors = random3Doors();
        
        //int firstIndex = ThreadLocalRandom.current().nextInt(doors.size());
        //Door firstGuess = doors.remove(firstIndex);
        
        // now reveal a goat
        // todo later: does it matter if the goat is shown at random
        int revealGoatIndex;
        do {
            revealGoatIndex = ThreadLocalRandom.current().nextInt(doors.size());
        } while (!doors.get(revealGoatIndex).contents.equals("Goat"));
        
        doors.remove(revealGoatIndex);
        
        Door finalChoice = doors.get(0);
        return finalChoice.contents.equals("Prize");
    }
    
    public boolean didGetPrizeChangingAfterRevealAlwaysShowFirstGoatEncountered() {
        List<Door> doors = random3Doors();
        
        int firstIndex = ThreadLocalRandom.current().nextInt(doors.size());
        doors.remove(firstIndex);
        
        // now reveal a goat
        // show the first remaining goat you see
        for (Door door : doors) {
            if (door.contents.equals("Goat")) {
                doors.remove(door);
                break;
            }
        }
        
        Door finalChoice = doors.get(0);
        return finalChoice.contents.equals("Prize");
    }
    
    public boolean didGetPrizeStayingFirmAfterReveal() {
        List<Door> doors = random3Doors();
        
        int firstIndex = ThreadLocalRandom.current().nextInt(doors.size());
        Door firstGuess = doors.remove(firstIndex);
        
        // now reveal a goat
        int revealGoatIndex;
        do {
            revealGoatIndex = ThreadLocalRandom.current().nextInt(doors.size());
        } while (!doors.get(revealGoatIndex).contents.equals("Goat"));
        
        doors.remove(revealGoatIndex);
        
        // stay with our first choice
        return firstGuess.contents.equals("Prize");
    }
    
    public boolean didGetPrizeNoReveal() {
        List<Door> doors = random3Doors();
        
        int firstIndex = ThreadLocalRandom.current().nextInt(doors.size());
        Door firstGuess = doors.remove(firstIndex);

        // stay with our first choice
        return firstGuess.contents.equals("Prize");
    }
}
