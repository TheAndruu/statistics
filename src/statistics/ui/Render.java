package statistics.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

public class Render {

	private static final String space = "&nbsp;";
	private boolean hasEqualSpacing = true;
	private int outputLength = 10;
	private JEditorPane jEditorPane;

	private StringBuilder builder;

	public void clear() {
		this.builder = new StringBuilder();
		builder.append("<html><body>");
		jEditorPane.setText("");
	}

	public void addRegular(int number) {
//		if (number %2 == 0 || number %5 == 0) {
//		return;
//	}
		//addRegular(df.format(number));
		addRegular(Integer.toBinaryString(number));
	}
	public void addRegular(String text) {
		if (hasEqualSpacing) {
			text = prependSpaces(text);
		}
		builder.append(asRegular(text));
	}

	private String prependSpaces(String text) {
		int originalTextLength = text.length();
		for (int i = outputLength-1; i >= originalTextLength; i--) {
			text = space + text;
		}
		return text;
	}
	public void addBold(int number) {
		//addBold(df.format(number));
		addBold(Integer.toBinaryString(number));
	}
	public void addBold(String text) {
		if (hasEqualSpacing) {
			text = prependSpaces(text);
		}
		builder.append(asBold(text));
	}

	public void addNewLine() {
		builder.append("<br />");
	}

	public void update() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				builder.append("</body></html>");
				String asString = builder.toString();
				jEditorPane.setText(asString);
				System.out.println(asString);
			}
		});
	}

	private String asBold(String text) {
		return "<span style='font: 16px Courier; font-weight: bold; color: green;'>" + text + " </span>";
	}

	private String asRegular(String text) {
		return "<span style='font: 16px Courier; color: gray;'>" + text + " </span>";
	}

	public Render() {
		this.builder = new StringBuilder();
		builder.append("<html><body>");
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// create jeditorpane
				jEditorPane = new JEditorPane();

				// make it read-only
				jEditorPane.setEditable(false);

				// create a scrollpane; modify its attributes as desired
				JScrollPane scrollPane = new JScrollPane(jEditorPane);

				// add an html editor kit
				HTMLEditorKit kit = new HTMLEditorKit();
				jEditorPane.setEditorKit(kit);

				// add some styles to the html
				StyleSheet styleSheet = kit.getStyleSheet();
				styleSheet.addRule("body {color:#000; margin: 4px; }");
//				styleSheet.addRule(".bold { font-weight: bold; }");
//				styleSheet.addRule("span {font: 12px Courier; display: inline-block; width: 60px;}");

				// create a document, set it on the jeditorpane, then add the html
				Document doc = kit.createDefaultDocument();
				jEditorPane.setDocument(doc);

				// now add it all to a frame
				JFrame j = new JFrame("HtmlEditorKit");
				j.getContentPane().add(scrollPane, BorderLayout.CENTER);

				// make it easy to close the application
				j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				// display the frame
				j.setSize(new Dimension(720, 640));

				// center the jframe, then make it visible
				j.setLocationRelativeTo(null);
				j.setVisible(true);
			}
		});
	}
}
