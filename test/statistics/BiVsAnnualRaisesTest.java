package statistics;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * "Suppose you have a job offer with a choice of two annual salaries. One is
 * $30,000 with a $1,000 raise every year. The other is $30,000 with a $300
 * raise every six months. Which option is best in the long run?" Savant claimed
 * that the semi-annual $300 raises were better than the annual $1000 raise.
 * 
 * from https://en.wikipedia.org/wiki/Marilyn_vos_Savant
 *
 */
class BiVsAnnualRaisesTest {

    private static final DecimalFormat df = new DecimalFormat("#,###,###.####");

    private long start;
    private long end;

    @BeforeEach
    void setup() {
        start = System.currentTimeMillis();
    }

    @AfterEach
    void teardown() {
        end = System.currentTimeMillis();
        System.out.println("Time took: " + (end - start) + " ms");
    }

    /** Returns an array of paycheck amounts for the specified params */
    private List<Double> calculatePay(double annualSalary, int monthsForARaise, double raiseAmountInDollars,
            int numYearsOfPay) {
        List<Double> monthlyPaychecks = new ArrayList<>(numYearsOfPay * 12);
        double monthlyPayRate = annualSalary / 12d;
        for (int yearNum = 0; yearNum < numYearsOfPay; yearNum++) {
            for (int month = 1; month <= 12; month++) {
                monthlyPaychecks.add(monthlyPayRate);
                if (month % monthsForARaise == 0) {
                    annualSalary += raiseAmountInDollars;
                    monthlyPayRate = annualSalary / 12d;
                }
            }

        }
        return monthlyPaychecks;
    }

//def collatz(number):
//   numIterations = 0
//   while(number != 1):
//     numIterations +=1
//     if (number %2 == 0):
//       number = number/2
//     else:
//       number = number*3+1
//     print "Number: " + str(number)
//   print "Iterations: " + str(numIterations)
//
//collatz(18)


    
    @Test
    void testAnnualRaises() {
        double startingSalary = 150_000d;
        int numYears = 2;
        int monthsToRaise = 6;
        double raiseAmount = 3000;
        List<Double> payForSixMonth300Raise= calculatePay(startingSalary, monthsToRaise, raiseAmount, numYears);
        printResults(payForSixMonth300Raise, startingSalary, monthsToRaise, raiseAmount, numYears );
        
        monthsToRaise = 12;
        raiseAmount = 10_000;
        List<Double> payForAnnual1000Raise= calculatePay(startingSalary, monthsToRaise, raiseAmount, numYears);
        printResults(payForAnnual1000Raise, startingSalary, monthsToRaise, raiseAmount, numYears );
    }

    private void printResults(List<Double> monthlyPaychecks, double startingSalary, int monthsToRaise,
            double raiseAmount, int numYears) {
        System.out.println(df.format(startingSalary) + " starting salary over " + df.format(numYears) + " year scenario");
        System.out.println("$" + df.format(raiseAmount) + " every " + df.format(monthsToRaise) + " months");
        
        double totalAmountMade = monthlyPaychecks.stream().reduce(Double::sum).get();
        System.out.println("Total amount made $" + df.format(totalAmountMade));
        
        double endingAnnualSalary = monthlyPaychecks.get(monthlyPaychecks.size()-1)*12;
        System.out.println("Ending annual salary: $" + df.format(endingAnnualSalary));
        
        System.out.println("Annualized salary at time of each raise:");
        int counter = 0;
        for (int i = monthsToRaise; i < monthlyPaychecks.size(); i+= monthsToRaise) {
            counter++;
            double annualizedSalary = monthlyPaychecks.get(i) * 12;
            System.out.println("  raise #" + df.format(counter) + " has annualized salary: $" + annualizedSalary + " on month #" + df.format(i));
            System.out.println("    amount over $"+ df.format(startingSalary) + " (starting salary) by: $" +df.format(annualizedSalary - startingSalary));
        }
        
        System.out.println();
        
        
        
    }

}
