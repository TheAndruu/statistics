package statistics;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import statistics.Blackjack.Card;

// Scenarios for retirement
// 
class BlackjackTest {

    private Blackjack blackjack;
    private DecimalFormat df = new DecimalFormat("###,###,###.##");
    private DecimalFormat percentageFormat = new DecimalFormat(".00000000");

    @BeforeEach
    void setup() {
        blackjack = new Blackjack();
    }

    @Test
    void testTrials_10_000_single_deck() {
        System.out.println("Single deck");
        testGettingBlackjack(10_000, 1);
    }

    @Test
    void testTrials_10_000_multiple_deck() {
        System.out.println("Six decks");
        testGettingBlackjack(10_000, 6);
    }

    
    void testGettingBlackjack(int numTrials, int numDecks) {

        int totalBlackjack = 0;
        int totalTrials = 0;
        List<Card> deck = blackjack.deal(numDecks);
        
        System.out.println("\n\nAttempts with " + df.format(numTrials) + " trials");
        for (int j = 0; j < 10; j++) {
            int numAttempts = 0;
            int numBlackjacks = 0;
            for (int i = 0; i < numTrials; i++) {
                Collections.shuffle(deck);
                numAttempts++;
                if (blackjack.getBlackjackFromShuffledDeck(deck)) {
                //if (blackjack.didGetBlackjackFromTwoRandomCards(deck)) {
                    numBlackjacks++;
                }
            }

            double percentage = numBlackjacks / ((double) numAttempts) * 100;
            System.out.print("Got " + df.format(numBlackjacks) + " blackjacks for " + df.format(percentage) + "%");
            System.out.println("  which equates to 1:" + df.format(numAttempts / (double) numBlackjacks));

            totalBlackjack += numBlackjacks;
            totalTrials += numAttempts;
        }
        System.out.println("\nOverall probability of blackjack: "
                + percentageFormat.format(totalBlackjack / (double) totalTrials));
    }
}
