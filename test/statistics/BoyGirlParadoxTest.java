package statistics;

import java.text.DecimalFormat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BoyGirlParadoxTest {

    private BoyGirlParadox genderAssignment;
    private static final DecimalFormat df = new DecimalFormat("#,###,###.####");
    
    private long start;
    private long end;
    
    @BeforeEach
    void setup() {
        genderAssignment = new BoyGirlParadox();
        start = System.currentTimeMillis();
    }
    
    @AfterEach
    void teardown() {
        end = System.currentTimeMillis();
        System.out.println("Time took: " + (end - start) + " ms");
    }
    
    @Test
    void testProbabilityOfKidGenders() {
        int iterations;
        int jonesKidsBothGirls = 0;
        int smithKidsBothBoys = 0;
        
        for (iterations = 0; iterations < 1_000_000; iterations++) {
            if (genderAssignment.areBothMrJonesKidsGirls()) {
                jonesKidsBothGirls++;
            }
            if (genderAssignment.areBothMrSmithsKidsBoys()) {
                smithKidsBothBoys++;
            }
        }
        System.out.println("Percentages after " + df.format(iterations) + ": ");
        System.out.println("  Jones kids both girls: " + df.format(jonesKidsBothGirls/(double)iterations*100));
        System.out.println("  Smith kids both boys : " + df.format(smithKidsBothBoys/(double)iterations*100));
    }

}
