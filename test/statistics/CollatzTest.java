package statistics;

import static java.lang.Math.sqrt;
import static java.util.stream.IntStream.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import statistics.ui.Render;

class CollatzTest extends TimedTest {

	Collatz collatz;

	@Override
	protected void beforeEach() {
		collatz = new Collatz();
	}

	@Disabled
	@Test
	void highlightPrimes() {
		AtomicInteger primeCounter = new AtomicInteger();
		int endNumber = 1000;
		rangeClosed(1, endNumber).forEach((i) -> {
			if (isPrime(i)) {
				System.out.print("_" + i + ",  ");
				primeCounter.incrementAndGet();
			} else {
				System.out.print(i + ",  ");
			}
		});
		System.out.println();
		System.out.println("Number of primes: " + primeCounter.get());
		System.out.println("Non-primes: " + (endNumber - primeCounter.get()));
	}

	// Find which numbers are used in the formula's iterations, and what's similar
	// about ones skippe
	@Test
	@Disabled
	void whatNumbersAreSkippedInIterations() {
		Set<Integer> uniqueNumbers = new TreeSet<>();
		int startNumber = 1;
		int endNumber = 1000;
		for (int i = startNumber; i <= endNumber; i++) {
			uniqueNumbers.addAll(collatz.getCollatzNumbers(i));
		}

		System.out.println("Number of unique divisors: " + uniqueNumbers.size());
		AtomicInteger primeCounter = new AtomicInteger();
		uniqueNumbers.stream().forEach((collatz) -> {
			// Collections.sort(runResults);
			if (isPrime(collatz)) {
				System.err.print(collatz + ", ");
				primeCounter.incrementAndGet();
			} else {
				System.out.print(collatz + ", ");
			}
		});
		System.out.println();
		System.out.println("Number of primes: " + primeCounter.get());
		System.out.println("Non-primes: " + (uniqueNumbers.size() - primeCounter.get()));
	}

	public static boolean isPrime(int n) {
		return n > 1 && rangeClosed(2, (int)sqrt(n)).noneMatch(divisor -> n % divisor == 0);

	}

	@Test
	void printPrimeNumbers() throws InterruptedException {
		Render render = new Render();
		
//		AtomicInteger primeCounter = new AtomicInteger();
		range(0, 1_000).forEach((number) -> {
			
			boolean isPrime = CollatzTest.isPrime(number);
			if (isPrime) {  
//				primeCounter.incrementAndGet();
				render.addBold(number);
			} else {
				render.addRegular(number);
			}
			if ((number +1) % 10 == 0) {
				render.addNewLine();
			}
			
//			if (number % 100 == 0) {
//				System.out.println("Primes leading up to " + number + ": "+ primeCounter.get());
//				primeCounter.set(0);
//			}
		});
		render.update();
		Thread.sleep(20000);
	}
		


	// if you do this for a large number of numbers.... will it only skip primes?
	// run tons of times, and see if the only numbers not outputted are primes
	@Disabled
	@Test
	void testPrintOrderedCollatzNumbers() {
		List<List<Integer>> allResults = new ArrayList<>();
		int startNumber = 1;
		int endNumber = 20;
		for (int i = startNumber; i < endNumber; i++) {
			List<Integer> results = collatz.getCollatzNumbers(i);
			allResults.add(results);
		}

//		for (int i = 99_995; i < endNumber-1; i++) {
//			System.out.print("n = " + df.format(i) + "\tIterations: " + allResults.get(i).size() + "\t");
//			System.out.println(allResults.get(i));
//		}
		int j = startNumber;
		for (List<Integer> runResults : allResults) {
			// Collections.sort(runResults);
			System.out.print("n = " + df.format(j++) + "\tIterations: " + runResults.size() + "\t");
			System.out.println(runResults);
		}
	}

	@Disabled
	@Test
	void testPrintUniqueCollatzNumbers() {
		Set<Integer> uniqueCollatz = new HashSet<>();
		for (int i = 1; i < 2; i++) {
			List<Integer> results = collatz.getCollatzNumbers(i);
			uniqueCollatz.addAll(results);
		}

		List<Integer> sorted = new ArrayList<>(uniqueCollatz);
		Collections.sort(sorted);
		System.out.println(sorted);

	}

}
