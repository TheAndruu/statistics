package statistics;

import java.util.List;

import org.junit.jupiter.api.Test;

import statistics.CollegeFund.Breakdown;

class CollegeFundTest extends TimedTest {

	private CollegeFund fund;
	@Override
	protected void beforeEach() {
	fund = new CollegeFund();	
	}
	
	@Test
	void testGetBreakdown() {
		List<Breakdown> results = fund.getBreakdown(30_000,  500, 14);
		results.forEach((result) -> System.out.println(result));
	}

}
