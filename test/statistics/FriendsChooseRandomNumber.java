package statistics;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * 4 friends (Alex, Blake, Chris and Dusty) each choose a random number between 1 and 5. What is the chance that any of them chose the same number?
 * 
 * from: 
 * https://www.mathsisfun.com/data/probability-events-conditional.html
 */
class FriendsChooseRandomNumber {

    boolean didFriendsPickSameNumber() {
        List<Integer> pickedNumbers = new ArrayList<>(4);
        for (int i = 0; i < 4; i++) {
            int randomNumber = ThreadLocalRandom.current().nextInt(1, 6);
            if (pickedNumbers.contains(randomNumber)) {
                return true;
            }
            pickedNumbers.add(randomNumber);
        }
        return false;
    }
    
    private static final DecimalFormat df = new DecimalFormat("#,###,###.####");
    
    private long start;
    private long end;
    
    @BeforeEach
    void setup() {
        start = System.currentTimeMillis();
    }
    
    @AfterEach
    void teardown() {
        end = System.currentTimeMillis();
        System.out.println("Time took: " + (end - start) + " ms");
    }
    
    @Test
    void testRandomNumberPicker() {
        int iterations;
        int pickedSameNumber = 0;
        
        for (iterations = 0; iterations < 1_000_000; iterations++) {
            if (didFriendsPickSameNumber()) {
                pickedSameNumber++;
            }
        }
        System.out.println("Percentages after " + df.format(iterations) + ": ");
        System.out.println("  Kids picked a matching number: " + df.format(pickedSameNumber/(double)iterations*100));
    }

}
