package statistics;

import java.text.DecimalFormat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MontyHallTest {

    private MontyHall monty;
    private static final DecimalFormat df = new DecimalFormat("#,###,###.####");
    
    private long start;
    private long end;
    
    @BeforeEach
    void setup() {
        monty = new MontyHall();
        start = System.currentTimeMillis();
    }
    
    @AfterEach
    void teardown() {
        end = System.currentTimeMillis();
        System.out.println("Time took: " + (end - start) + " ms");
    }
    @Test
    void testMontyHall() {
        int iterations;
        int winAfterChanging = 0;
        int winAfterChangingNonRandomReveal = 0;
        int winWithoutChanging = 0;
        int winWithoutReveal = 0;
        
        for (iterations = 0; iterations < 1_000_000; iterations++) {
            if (monty.didGetPrizeChangingAfterReveal()) {
                winAfterChanging++;
            }
            if (monty.didGetPrizeChangingAfterRevealAlwaysShowFirstGoatEncountered()) {
                winAfterChangingNonRandomReveal++;
            }
            if (monty.didGetPrizeNoReveal()) {
                winWithoutReveal++;
            }
            if (monty.didGetPrizeStayingFirmAfterReveal()) {
                winWithoutChanging++;
            }
        }
        System.out.println("Winning percentages after " + df.format(iterations) + ": ");
        System.out.println("  without changing original pick  : " + df.format(winWithoutChanging/(double)iterations*100));
        System.out.println("  without reveal                  : " + df.format(winWithoutReveal/(double)iterations*100));
        System.out.println("  with changing after reveal      : " + df.format(winAfterChanging/(double)iterations*100));
        System.out.println("  changing after reveal (non-rand): " + df.format(winAfterChangingNonRandomReveal/(double)iterations*100));
        
    }

}
