package statistics;

import java.text.DecimalFormat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

public class TimedTest {

    protected static final DecimalFormat df = new DecimalFormat("#,###,###.####");

    protected long start;
    protected long end;

    @BeforeEach
    protected void _beforeEach() {
        start = System.currentTimeMillis();
        beforeEach();
    }
    
    protected void beforeEach() {
    	
    }
    protected void afterEach() { 
    	
    }

    @AfterEach
    protected void _afterEach() {
        end = System.currentTimeMillis();
        System.out.println("\nTime took: " + (end - start) + " ms");
        afterEach();
    }

}
